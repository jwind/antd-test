import React, {Component} from 'react';
import {Table, Input, Popconfirm, Row, Col} from 'antd';
import data from './list.json';
import 'antd/dist/antd.css';
import './App.css';
const Search = Input.Search;

class App extends Component {
    state = {
        data: data
    };
    //通过名字删除
    deleteByName = (val) => {
        for (let i = 0; i < data.length; i++) {
            if (val === data[i].name) {
                data.splice(i, 1);
                this.setState({
                    data: data
                });
            }
        }
    };
    //分页改变，
    handleChange = (pagination, filters, sorter) => {
        console.log(pagination, filters, sorter);
    }
    //搜索条件
    search = (val) => {
        if (val === undefined) {
            return false;
        }
        const list = [];
        for (let i = 0; i < data.length; i++) {
            if (-1 < data[i].name.indexOf(val)) {
                list.push(data[i])
            }
        }
        this.setState({
            data: list
        });
    }

    render() {
        //表头
        const columns = [{
            title: '姓名',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => {
                let nameA = a.name.toUpperCase();
                let nameB = b.name.toUpperCase();
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }

                // names must be equal
                return 0;
            },
        }, {
            title: '日期',
            dataIndex: 'date',
            key: 'date',
        }, {
            title: '分组',
            dataIndex: 'group',
            key: 'group',
            filters: [
                {text: 'BS', value: 'BS'},
                {text: 'CS', value: 'CS'},
            ],
            onFilter: (value, record) => record.group.includes(value),
        }, {
            title: '操作',
            dataIndex: 'opt',
            key: 'opt',
            render: (text, record) => (
                <Popconfirm title="Are you sure delete this task?" onConfirm={() => this.deleteByName(record.name)}
                            onCancel={() => console.log('cancel!!')} okText="Yes" cancelText="No">
                    <a>Delete</a>
                </Popconfirm>
            )
        }];
        //分页每页显示
        const pagination = {
            pageSize: 5,
        }
        return (
            <div style={{marginLeft: 30, marginRight: 'auto',}}>
                <div style={{marginTop:50}}>
                    <Row>
                        <Col span={8}/>
                        <Col span={8}>
                            <Search placeholder="input search text"
                                    onSearch={value => this.search(value)}
                                    enterButton="Search"/>
                        </Col>
                    </Row>
                </div>
                <Row>
                    <Col span={22}>
                        <div style={{marginTop: 20}}>
                            <Table columns={columns} dataSource={this.state.data} onChange={this.handleChange}
                                   pagination={pagination}/>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default App;
